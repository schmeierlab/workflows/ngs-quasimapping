This workflow performs uses salmon to do quasi-mapping of single- or paired-end RNA-seq data to a transcriptome. In addition, the workflow allows to do differential expression analysis.

Adapter/quality trimming was performed with `BBDuk <https://jgi.doe.gov/data-and-tools/bbtools/bb-tools-user-guide/bbduk-guide/>`_ and an adapter collection from `here <https://raw.githubusercontent.com/sschmeier/seq_adapters_contaminants/master/adapters_union.fa>`_. 

Gene counts were generated with `Salmon <https://salmon.readthedocs.io/en/latest/salmon.html>`_.

Integrated normalization and differential expression analysis was conducted with `DESeq2 <https://bioconductor.org/packages/release/bioc/html/DESeq2.html>`_.
