"""Snakemake wrapper for Salmon Quant
Adjusted from https://bitbucket.org/snakemake/snakemake-wrappers/raw/b48c47ecd268be992730c7abe0682e091eb1427c/bio/salmon/quant/wrapper.py
"""

__author__ = "Sebastian Schmeier"
__copyright__ = "Copyright 2018, Sebastian Schmeier"
__email__ = "s.schmeier@gmail.com"
__license__ = "MIT"

from os import path
from snakemake.shell import shell

def auto_decompression(reads):
    """ Allow *.bz2 input into salmon. Also provide same
    decompression for *gz files, as salmon devs mention
    it may be faster in some cases."""
    zip_ext =  reads.split('.')[-1]
    if zip_ext == 'bz2':
        reads = ' <(bunzip2 -c ' + reads + ')'
    elif zip_ext == 'gz':
        reads = ' <(gunzip -c ' + reads + ')'
    else:
        reads = reads
    return reads

extra = snakemake.params.get("extra", "")
log = snakemake.log_fmt_shell(stdout=False, stderr=True)
libtype = snakemake.params.get("libtype", "A")

sample = snakemake.input.get("sample")
fq1 = sample[0]
assert fq1 is not None, "input-> fq1 is a required input parameter"

if len(sample) == 2: ## paired end
    fq2 = sample[1]
    read_cmd = ' -1 ' + auto_decompression(fq1) + ' -2 ' + auto_decompression(fq2)
else: ## single end
    read_cmd = ' -r ' + auto_decompression(fq1)

outdir = path.dirname(snakemake.output.get('quant'))

shell("salmon quant -i {snakemake.input.index} "
      " -l {libtype} {read_cmd} -o {outdir} "
      " -p {snakemake.threads} {extra} {log} ")
