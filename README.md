# Snakemake workflow: SALMON-based quasi-mapping

[![pipeline status](https://gitlab.com/schmeierlab/workflows/ngs-quasimapping/badges/v0.10.1/pipeline.svg)](https://gitlab.com/schmeierlab/workflows/ngs-quasimapping/-/commits/v0.10.1)

- AUTHOR: Sebastian Schmeier (s.schmeier@pm.me)
- DATE: 2019
- VERSION: 0.10.1

[[_TOC_]]

## 0. Install snakemake and clone this repo

> We are **not** using the latest Snakemake version (5.10.0) as it as problems with Singularity args.

```bash
conda create --yes -n snakemake snakemake=5.9.1 
conda activate snakemake
```

```bash
git clone git@gitlab.com:schmeierlab/workflows/ngs-quasimapping.git
# or
git clone https://gitlab.com/schmeierlab/workflows/ngs-quasimapping.git
```

## 1. Download transcriptome and gtf annotation (and genome, if decoys should be used)

For example one can use data from Gencode:

```bash
mkdir -p data/gencode
cd data/gencode

curl -O ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_33/gencode.v33.transcripts.fa.gz

curl -O ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_33/gencode.v33.primary_assembly.annotation.gtf.gz

curl -O ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_33/GRCh38.p13.genome.fa.gz
```

### 1.1 Prepare decoy sequences and gentrome

See [https://combine-lab.github.io/alevin-tutorial/2019/selective-alignment/](https://combine-lab.github.io/alevin-tutorial/2019/selective-alignment/).

```bash
cd data/gencode

grep "^>" <(zcat GRCh38.p13.genome.fa.gz) | cut -d " " -f 1 | sed 's/>//g' > decoys.txt

# concat genome after txome (not the other way around)
cat gencode.v33.transcripts.fa.gz GRCh38.p13.genome.fa.gz > gentrome.fa.gz
```

## 2. Prepare salmon index

### 2.1 Build your own index

[Salmon](https://salmon.readthedocs.io/en/latest/salmon.html) is used in the workflow.
You can build the index for your transcriptome/gentrome like so:

```bash
# without decoys
salmon index -p 32 -t data/gencode/gencode.v33.transcripts.fa.gz -i idx --gencode

# with decoys
salmon index -p 32 -t data/gencode/gentrome.fa.gz -i idx -d data/gencode/decoys.txt --gencode
```

The `--gencode` flag is only relevant if [Gencode](https://www.gencodegenes.org/human/release_33.html) data is used.

We can use singularity for it too, e.g.

```bash
singularity pull shub://sschmeier/simg-rnaseq:202002

singularity exec --bind "/mnt/DATA" simg-rnaseq_202002.sif salmon index -p 32 -t data/gencode/gentrome.fa.gz -i idx -d data/gencode/decoys.txt --gencode
```

### 2.2 Download pre-build indeces

For example ,from refgenie for [hg38](http://refgenomes.databio.org/v3/assets/splash/2230c535660fb4774114bfa966a62f823fdb6d21acf138d4/salmon_sa_index?tag=default).


## 3. Adjust the config file where necessary

Point to the correct samplesheet, transcriptome index and annotation.
Change the contrasts if differential expression analysis should be performed.

> `--configfile <file>` needs to be specified as a `snakemake` argument.


## 4. Running the workflow

### 4.1 Singularity-only mode (recommended)

Change the `singularity` variable in the `config.yaml` to `singularity: shub://sschmeier/simg-rnaseq:202002`

```bash
snakemake -p --use-singularity --singularity-args "--bind /mnt/DATA" --jobs 32 --configfile config.yaml 2> run.log
```

### 4.2 Singularity + Conda  mode

Change the `singularity` variable in the `config.yaml` to `singularity: docker://continuumio/miniconda3`

```bash
snakemake -p --use-conda --use-singularity --singularity-args "--bind /mnt/DATA" --jobs 32 --configfile config.yaml 2> run.log
```

### 4.3 Conda-only mode

```bash
snakemake -p --use-conda --jobs 32 --configfile config.yaml 2> run.log 
```


## 5. Create report

```bash
snakemake --configfile config.yaml --report report.html
```


## Test

```bash
git clone https://gitlab.com/schmeierlab/workflows/ngs-test-data.git
salmon index -p 32 -t ngs-test-data/ref/transcriptome/Homo_sapiens.GRCh38.cdna.21.fa -i ngs-test-data/ref/transcriptome/Homo_sapiens.GRCh38.cdna.21.fa.idx 
snakemake -p --use-singularity --configfile config.yaml
# or
snakemake -p --use-conda --configfile config.yaml
```


## Cluster execution (NESI Mahuika)

### Conda-mode

```bash
# create one global env
conda env create -n rnaseq -f data/nesi/rnaseq-nesi-mahuika.yaml

# activate env
conda activate rnaseq

# run snakemake in global env
snakemake -j 999 --cluster-config data/nesi/cluster-nesi-mahuika.yaml --cluster "sbatch -A {cluster.account} -p {cluster.partition} -n {cluster.ntasks} -t {cluster.time} --hint={cluster.hint} --output={cluster.output} --error={cluster.error} -c {cluster.cpus-per-task}" -p --rerun-incomplete --configfile config.yaml
```

### Singularity-mode

```bash
snakemake --use-singularity --singularity-args "--bind /scale_wlg_nobackup/filesets/nobackup/massey02702" -j 999 --cluster-config data/nesi/cluster-nesi-mahuika.yaml --cluster "sbatch -A {cluster.account} -p {cluster.partition} -n {cluster.ntasks} -t {cluster.time} --hint={cluster.hint} --output={cluster.output} --error={cluster.error} -c {cluster.cpus-per-task}" -p --rerun-incomplete --configfile config.yaml
```
