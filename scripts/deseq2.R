log <- file(snakemake@log[[1]], open="wt")
sink(log)
sink(log, type="message")

library("DESeq2")

parallel <- FALSE
if (snakemake@threads > 1) {
    library("BiocParallel")
    # setup parallelization
    register(MulticoreParam(snakemake@threads))
    parallel <- TRUE
}

dds <- readRDS(snakemake@input[[1]])

contrast <- c("condition", snakemake@params[["contrast"]])
# Using ihw?
if (snakemake@params[["filterFun"]] == "ihw") {
  library("IHW")
  res <- results(dds, contrast=contrast, parallel=parallel, filterFun=ihw)
} else {
  res <- results(dds, contrast=contrast, parallel=parallel)
}

# shrink fold changes for lowly expressed genes
res <- lfcShrink(dds, contrast=contrast, res=res)
# sort by p-value
res <- res[order(res$padj),]

# store results
pdf(snakemake@output[["ma_plot"]])
plotMA(res, ylim=c(-2,2))
dev.off()

gz <- gzfile(snakemake@output[["table"]], "w")
header <- t(c("gene", as.character(colnames(res))))
write.table(header, file=gz, row.names=FALSE, col.names=FALSE, quote=FALSE, sep="\t")
write.table(res, file=gz, row.names=TRUE, col.names=FALSE, quote=FALSE, sep="\t")
close(gz)
