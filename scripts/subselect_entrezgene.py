#!/usr/bin/env python
"""
NAME: subselect_entrezgene
==========================

DESCRIPTION
===========

INSTALLATION
============

USAGE
=====

VERSION HISTORY
===============

0.0.2   20191216    Based on tx_gene_map now
0.0.1   20171209    Initial version.

LICENCE
=======
2017-2019, copyright Sebastian Schmeier (s.schmeier@gmail.com)

template version: 1.6 (2016/11/09)
"""
from timeit import default_timer as timer
from signal import signal, SIGPIPE, SIG_DFL
import sys
import os
import os.path
import argparse
import csv
import collections
import gzip
import bz2
import zipfile
import time

# When piping stdout into head python raises an exception
# Ignore SIG_PIPE and don't throw exceptions on it...
# (http://docs.python.org/library/signal.html)
signal(SIGPIPE, SIG_DFL)

__version__ = '0.0.2'
__date__ = '20191216'
__email__ = 's.schmeier@pm.me'
__author__ = 'Sebastian Schmeier'


def parse_cmdline():
    """ Parse command-line args. """
    ## parse cmd-line -----------------------------------------------------------
    description = 'Subselect genes from tpm table that have an non-redundant entrez id. Some Ensembl/Gencode genes have same Entrez GeneId.'
    version = 'version %s, date %s' % (__version__, __date__)
    epilog = 'Copyright %s (%s)' % (__author__, __email__)

    parser = argparse.ArgumentParser(description=description, epilog=epilog)

    parser.add_argument('--version',
                        action='version',
                        version='%s' % (version))

    parser.add_argument(
        'str_file',
        metavar='FILE',
        help=
        'Delimited expression table. [if set to "-" or "stdin" reads from standard in]')
    parser.add_argument(
        'str_ref',
        metavar='FILE',
        help= 'tx_gene_map_entrez.txt')
    parser.add_argument('-o',
                        '--out',
                        metavar='STRING',
                        dest='outfile_name',
                        default=None,
                        help='Out-file. [default: "stdout"]')

    # if no arguments supplied print help
    if len(sys.argv)==1:
        parser.print_help()
        sys.exit(1)
    
    args = parser.parse_args()
    return args, parser


def load_file(filename):
    """ LOADING FILES """
    if filename in ['-', 'stdin']:
        filehandle = sys.stdin
    elif filename.split('.')[-1] == 'gz':
        filehandle = gzip.open(filename, 'rt')
    elif filename.split('.')[-1] == 'bz2':
        filehandle = bz2.BZFile(filename)
    elif filename.split('.')[-1] == 'zip':
        filehandle = zipfile.Zipfile(filename)
    else:
        filehandle = open(filename)
    return filehandle

        
def main():
    """ The main funtion. """
    args, parser = parse_cmdline()
    try:
        fileobj = load_file(args.str_file)
    except:
        error("Coul not load file {}".format(args.str_file))

    try:
        fileref = load_file(args.str_ref)
    except:
        error("Coul not load file {}".format(args.str_ref))

    # create outfile object
    if not args.outfile_name:
        outfileobj = sys.stdout
    elif args.outfile_name in ['-', 'stdout']:
        outfileobj = sys.stdout
    elif args.outfile_name.split('.')[-1] == 'gz':
        outfileobj = gzip.open(args.outfile_name, 'wt')
    else:
        outfileobj = open(args.outfile_name, 'wt')
        
    reader = csv.reader(fileobj, delimiter = '\t')
    readerref = csv.reader(fileref, delimiter = '\t')
    ref = {}
    for a in readerref:
        ref[a[1]] = a[3]
    fileref.close()

    header = next(reader)
    
    idx = {}
    for a in reader:
        geneid = a[0]
        try:
            entrezid = ref[geneid]
        except KeyError:
            continue
        idx[entrezid] = idx.get(entrezid, []) + [geneid]
    fileobj.close()
        
    # second time trough file, more mem efficient than using lists
    reader = csv.reader(load_file(args.str_file), delimiter = '\t')
    header = next(reader)
    outfileobj.write("{}\n".format("\t".join(list(header))))
    for a in reader:
        geneid = a[0]
        try:
            entrezid = ref[geneid]
        except KeyError:
            continue
        # if 1 to 1 in file print
        if len(idx[entrezid]) == 1:
            a[0] = entrezid
            outfileobj.write('{}\n'.format('\t'.join(a)))
        
    return


if __name__ == '__main__':
    sys.exit(main())

