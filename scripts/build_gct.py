#!/usr/bin/env python
"""
NAME: build_gct
=========

DESCRIPTION
===========

From a matrix of expression values and a GTF file, build a GCT expression file for
visualization in IGV.

http://software.broadinstitute.org/cancer/software/genepattern/file-formats-guide#GCT
http://software.broadinstitute.org/software/igv/GCT

INSTALLATION
============

USAGE
=====

VERSION HISTORY
===============

0.0.1    2018    Initial version.

LICENCE
=======
2017, copyright Sebastian Schmeier, (s.schmeier@gmail.com), https://sschmeier.com

template version: 1.9 (2017/12/08)
"""
from signal import signal, SIGPIPE, SIG_DFL
import sys
import os
import os.path
import argparse
import csv
import collections
import gzip
import bz2
import zipfile
import time
import re

# When piping stdout into head python raises an exception
# Ignore SIG_PIPE and don't throw exceptions on it...
# (http://docs.python.org/library/signal.html)
signal(SIGPIPE, SIG_DFL)

__version__ = '0.0.1'
__date__ = '2018'
__email__ = 's.schmeier@gmail.com'
__author__ = 'Sebastian Schmeier'

# For color handling on the shell
try:
    from colorama import init, Fore, Style
    # INIT color
    # Initialise colours for multi-platform support.
    init()
    reset=Fore.RESET
    colors = {'success': Fore.GREEN, 'error': Fore.RED, 'warning': Fore.YELLOW, 'info':''}
except ImportError:
    sys.stderr.write('colorama lib desirable. Install with "conda install colorama".\n\n')
    reset=''
    colors = {'success': '', 'error': '', 'warning': '', 'info':''}

def alert(atype, text, log):
    textout = '%s [%s] %s\n' % (time.strftime('%Y%m%d-%H:%M:%S'),
                                atype.rjust(7),
                                text)
    log.write('%s%s%s' % (colors[atype], textout, reset))
    if atype=='error': sys.exit(1)
        
def success(text, log=sys.stderr):
    alert('success', text, log)
    
def error(text, log=sys.stderr):
    alert('error', text, log)
    
def warning(text, log=sys.stderr):
    alert('warning', text, log)
    
def info(text, log=sys.stderr):
    alert('info', text, log)
    
##----------------------------------------------------------


def parse_cmdline():
    # parse cmd-line -----------------------------------------------------------
    sDescription = 'Statistics for each tx. Output: num_elements, num_zero, num_non-zero, num_greaterequal1, num_unique, max, min, sum, mean, median, std, var' 
    sVersion='version %s, date %s' %(__version__,__date__)
    sEpilog = 'Copyright %s (%s)' %(__author__, __email__)

    parser = argparse.ArgumentParser(description=sDescription,
                                      epilog=sEpilog)
    parser.add_argument('--version',
                        action='version',
                        version='%s' % (sVersion))
    parser.add_argument('str_file1',
                         metavar='FILE',
                         help='Expression matrix.')
    parser.add_argument('str_file2',
                         metavar='FILE',
                         help='GTF-file reference')
    parser.add_argument('-d', '--delimiter',
                         metavar='STRING',
                         dest='sDelim',
                         default='\t',
                         help='Delimiter for the columns of the expression matrix file. [default: tab]')
    parser.add_argument('-o',
                        '--out',
                        metavar='STRING',
                        dest='outfile_name',
                        default=None,
                        help='Out-file. [default: "stdout"]')
    parser.add_argument('--gene',
                        action="store_true",
                        dest='genemode',
                        default=False,
                        help='Use genes instead of tx.')
    parser.add_argument('--noversion',
                        action="store_true",
                        dest='noversion',
                        default=False,
                        help='Do not use gene/tx versions. [default: "False"]')
    
    # if no arguments supplied print help
    if len(sys.argv)==1:
        parser.print_help()
        sys.exit(1)
    
    args = parser.parse_args()
    return args, parser


def load_file(filename):
    """ LOADING FILES """
    if filename in ['-', 'stdin']:
        filehandle = sys.stdin
    elif filename.split('.')[-1] == 'gz':
        filehandle = gzip.open(filename, 'rt')
    elif filename.split('.')[-1] == 'bz2':
        filehandle = bz2.BZFile(filename)
    elif filename.split('.')[-1] == 'zip':
        filehandle = zipfile.Zipfile(filename)
    else:
        filehandle = open(filename)
    return filehandle

def main():
    args, parser = parse_cmdline()

    try:
        fileobj1 = load_file(args.str_file1)
    except:
        error('Could not load file 1 {}. EXIT.'.format(args.str_file1))

    try:
        fileobj2 = load_file(args.str_file2)
    except:
        error('Could not load file 2 {}. EXIT.'.format(args.str_file2))
    
    # create outfile object
    if not args.outfile_name:
        outfileobj = sys.stdout
    elif args.outfile_name in ['-', 'stdout']:
        outfileobj = sys.stdout
    elif args.outfile_name.split('.')[-1] == 'gz':
        outfileobj = gzip.open(args.outfile_name, 'wt')
    else:
        outfileobj = open(args.outfile_name, 'w')


    if args.genemode:
        regex = re.compile('gene_id\s+"(.+?)";')
        cmptype = 'gene'
    else:
        regex  = re.compile('transcript_id\s+"(.+?)";.+transcript_version\s+"(.+?)";')
        regex2 = re.compile('transcript_id\s+"(.+?)";')
        cmptype = 'transcript'
        
    iID = 0
    d = {}
    for line in fileobj2:
        if line[0] == '#': continue
        
        a = [s.strip() for s in line.split('\t')]
        
        res = regex.search(a[8])
        if not res:
            if cmptype == "transcript":
                res = regex2.search(line)
                if not res:
                    #print(a[8])
                    continue
                    
            else:
                continue

        if cmptype == "transcript":
            if len(res.groups()) > 1:
                #print(res.groups())
                idx = '%s.%s'%res.groups()
            else:
                idx = res.group(1)
        else:
            idx = res.group(1)

        if args.noversion:
            idx = idx.split(".")[0]
            
        chr = a[0]
        start = a[3]
        stop = a[4]
        desc = '%s |@%s:%s-%s|' % (idx, chr, start, stop)
        d[idx] = desc


    #print(len(d))
        
    # encode location in DESCRIPTION string of gct file
    # http://software.broadinstitute.org/software/igv/GCT
    # http://software.broadinstitute.org/cancer/software/genepattern/file-formats-guide#GCT
    csv_reader = csv.reader(fileobj1, delimiter = '\t')
    header = next(csv_reader)
    outfileobj.write("#1.2\n%i\t%i\n" % (iID, len(header)-1))
    outfileobj.write("%s\tDESCRIPTION\t%s\n" % (header[0], "\t".join(header[1:])))
    for a in csv_reader:
        idx = a[0]
        #print(idx)
        if idx not in d:
            warning("idx {} not found in reference".format(idx))
            continue
        
        outfileobj.write("%s\t%s\t%s\n" % (a[0], d[idx], '\t'.join(a[1:])))
        
    return

if __name__ == '__main__':
    sys.exit(main())
