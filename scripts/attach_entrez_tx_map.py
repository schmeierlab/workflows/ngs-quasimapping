#!/usr/bin/env python
"""
NAME: attach_entrez_tx_map
==========================

DESCRIPTION
===========

INSTALLATION
============

USAGE
=====

VERSION HISTORY
===============

0.0.1    20191216    Initial version.

LICENCE
=======
2017, copyright Sebastian Schmeier, (s.schmeier@gmail.com), https://sschmeier.com

template version: 1.9 (2017/12/08)
"""
from signal import signal, SIGPIPE, SIG_DFL
import sys
import os
import os.path
import argparse
import csv
import collections
import gzip
import bz2
import zipfile
import time
import re

# When piping stdout into head python raises an exception
# Ignore SIG_PIPE and don't throw exceptions on it...
# (http://docs.python.org/library/signal.html)
signal(SIGPIPE, SIG_DFL)

__version__ = '0.0.1'
__date__ = '20191216'
__email__ = 's.schmeier@pm.me'
__author__ = 'Sebastian Schmeier'

# For color handling on the shell
try:
    from colorama import init, Fore, Style
    # INIT color
    # Initialise colours for multi-platform support.
    init()
    reset=Fore.RESET
    colors = {'success': Fore.GREEN, 'error': Fore.RED, 'warning': Fore.YELLOW, 'info':''}
except ImportError:
    sys.stderr.write('colorama lib desirable. Install with "conda install colorama".\n\n')
    reset=''
    colors = {'success': '', 'error': '', 'warning': '', 'info':''}

def alert(atype, text, log, repeat=False):
    if repeat:
        textout = '%s [%s] %s\r'%(time.strftime('%Y%m%d-%H:%M:%S'),
                                        atype.rjust(7),
                                        text)
    else:
        textout = '%s [%s] %s\n'%(time.strftime('%Y%m%d-%H:%M:%S'),
                                        atype.rjust(7),
                                        text)

    
    log.write('%s%s%s'%(colors[atype], textout, reset))
    if atype=='error': sys.exit(1)

def success(text, log=sys.stderr):
    alert('success', text, log)

def error(text, log=sys.stderr):
    alert('error', text, log)

def warning(text, log=sys.stderr):
    alert('warning', text, log)
    
def info(text, log=sys.stderr, repeat=False):
    alert('info', text, log)  

    
def parse_cmdline():
    """ Parse command-line args. """
    ## parse cmd-line -----------------------------------------------------------
    description = 'Read tx_gene_map and extract per tx the entrez gene id from prop[er annotation,'
    version = 'version %s, date %s'%(__version__, __date__)
    epilog = 'Copyright %s (%s)'%(__author__, __email__)

    parser = argparse.ArgumentParser(description=description, epilog=epilog)

    parser.add_argument('--version',
                        action='version',
                        version='%s'%(version))

    parser.add_argument(
        'str_file',
        metavar='FILE',
        help=
        'tx_gene_map.')
    parser.add_argument(
        'str_file_anno',
        metavar='FILE',
        help=
        'Annotation from either Ensembl (ftp://ftp.ncbi.nih.gov/gene/DATA/gene2ensembl.gz) or Gencode (ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_32/gencode.v32.metadata.EntrezGene.gz).')
    parser.add_argument('-t',
                        '--type',
                        metavar='STRING',
                        dest='type',
                        default="ensembl",
                        help='Type of annotation, either ensembl (default) or gencode.')
    parser.add_argument('-o',
                        '--out',
                        metavar='STRING',
                        dest='outfile_name',
                        default=None,
                        help='Out-file. [default: "stdout"]')
    
    # if no arguments supplied print help
    if len(sys.argv)==1:
        parser.print_help()
        sys.exit(1)
    
    args = parser.parse_args()
    return args, parser


def load_file(filename):
    """ LOADING FILES """
    if filename in ['-', 'stdin']:
        filehandle = sys.stdin
    elif filename.split('.')[-1] == 'gz':
        filehandle = gzip.open(filename, 'rt')
    elif filename.split('.')[-1] == 'bz2':
        filehandle = bz2.open(filename, 'rt')
    elif filename.split('.')[-1] == 'zip':
        filehandle = zipfile.ZipFile(filename)
    else:
        filehandle = open(filename)
    return filehandle


def main():
    """ The main funtion. """
    args, parser = parse_cmdline()

    try:
        fileobj = load_file(args.str_file_anno)
    except:
        error('Could not load file {}. EXIT.'.format(args.str_file_anno))
        
    # create outfile object
    if not args.outfile_name:
        outfileobj = sys.stdout
    elif args.outfile_name in ['-', 'stdout']:
        outfileobj = sys.stdout
    elif args.outfile_name.split('.')[-1] == 'gz':
        outfileobj = gzip.open(args.outfile_name, 'wt')
    else:
        outfileobj = open(args.outfile_name, 'w')

    ref = {}
    if args.type == 'ensembl':
        tx_col = 4
        entrez_col = 1
    elif args.type == 'gencode':
        tx_col = 0
        entrez_col = 1 
    else:
        error('Type "{}" not recognised. Either ensembl or gencode.'.format(args.type))

    reader = csv.reader(fileobj, delimiter = "\t")
    for a in reader:
        # tx => entrez
        if a[tx_col] in ref:
            warning("Double tx entry seen: {}".format(a[tx_col]))
        ref[a[tx_col]] = a[entrez_col]

    # now process tx gene map
    try:
        fileobj = load_file(args.str_file)
    except:
        error('Could not load file {}. EXIT.'.format(args.str_file))
        
    reader = csv.reader(fileobj, delimiter = "\t")
    for a in reader:
        try:
            entrez = ref[a[0]]
        except KeyError:
            warning("Tx {} not in reference annotation.".format(a[0]))
            continue
        
        outfileobj.write('%s\t%s\n'%("\t".join(a), entrez))
    fileobj.close()      
    

    # ------------------------------------------------------
    outfileobj.close()
    return


if __name__ == '__main__':
    sys.exit(main())

