## ===================================================================
## bbduk adapter and quality trimming
## only performed if requested.
rule bbduk_pe:
    input:
        get_fastq
    output:
        out1=join(DIR_TRIM, "{sample}.1.fastq.gz"),
        out2=join(DIR_TRIM, "{sample}.2.fastq.gz")
    log:
        join(DIR_LOGS, "bbduk_pe/{sample}.log")
    benchmark:
        join(DIR_BENCHMARKS,"{sample}.bbduk_pe.txt")
    threads: 8
    conda:
        join(DIR_ENVS, "bbmap.yaml")
    params:
        extra=config["bbduk"]["extra"],
        adapters=abspath(config["bbduk"]["adapters"]),
        stats=join(DIR_TRIM, "{sample}.stats")
    shell:
        "bbduk.sh {params.extra} "
        "ref={params.adapters} "
        "stats={params.stats} "
        "in1={input[0]} "
        "in2={input[1]} "
        "out1={output.out1} "
        "out2={output.out2} "
        "> {log} 2>&1"


rule bbduk_se:
    input:
        get_fastq
    output:
        out=join(DIR_TRIM, "{sample}.fastq.gz")
    log:
        join(DIR_LOGS, "bbduk_se/{sample}.log")
    benchmark:
        join(DIR_BENCHMARKS,"{sample}.bbduk_se.txt")
    threads: 8
    conda:
        join(DIR_ENVS, "bbmap.yaml")
    params:
        extra=config["bbduk"]["extra"],
        adapters=abspath(config["bbduk"]["adapters"]),
        stats=join(DIR_TRIM, "{sample}.stats")
    shell:
        "bbduk.sh {params.extra} "
        "ref={params.adapters} "
        "stats={params.stats} "
        "in={input[0]} "
        "out={output.out} "
        "> {log} 2>&1"

