## ===================================================================
## fastp trimming
rule fastp_se:
    input:
        sample=get_fastq
    output:
        trimmed=join(DIR_TRIM, "{sample}.fastq.gz"),
        html=report(join(DIR_TRIM, "{sample}.fastp.html")),
        json=join(DIR_TRIM, "{sample}.fastp.json")
    log:
        join(DIR_LOGS, "fastp_se/{sample}.log")
    benchmark:
        join(DIR_BENCHMARKS,"{sample}.fastp_se.txt")
    params:
        extra=""
    threads: 8
    conda:
        join(DIR_ENVS, "fastp.yaml")
    wrapper:
        "file://wrapper/fastp"


rule fastp_pe:
    input:
        sample=get_fastq
    output:
        trimmed=[join(DIR_TRIM, "{sample}.1.fastq.gz"), join(DIR_TRIM, "{sample}.2.fastq.gz")],
        html=report(join(DIR_TRIM, "{sample}.fastp.html")),
        json=join(DIR_TRIM, "{sample}.fastp.json")
    log:
        join(DIR_LOGS, "fastp_pe/{sample}.log")
    benchmark:
        join(DIR_BENCHMARKS,"{sample}.fastp_pe.txt")
    params:
        extra=""
    threads: 8
    conda:
        join(DIR_ENVS, "fastp.yaml")
    wrapper:
        "file://wrapper/fastp"
