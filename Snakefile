import pandas as pd
import glob, os, os.path, datetime, sys, csv
from os.path import join, abspath
from snakemake.utils import validate, min_version

##### set minimum snakemake version #####
min_version("5.16.0")

## =============================================================================
## SETUP
## =============================================================================

# DIRS
DIR_SCHEMAS = abspath("schemas")
DIR_SCRIPTS = abspath("scripts")
DIR_REPORT  = abspath("report")
DIR_ENVS    = abspath("envs")
DIR_RULES   = abspath("rules")

report: join(DIR_REPORT, "workflow.rst")

##### load config and sample sheets #####
# This needs to be commented out for now as there is a bug
# https://bitbucket.org/snakemake/snakemake/issues/1184/configfile-does-not-replace-snakefile
# thats merges configs...
##configfile: "config.yaml"

if config=={}:
    print('Please submit config-file with "--configfile <file>". Exit.')
    sys.exit(1)

# specify config file with --configfile
# validate config file
validate(config, schema=join(DIR_SCHEMAS, "config.schema.yaml"))
## print the config to stderr
sys.stderr.write("********** Submitted config: **********\n")
for k,v in config.items():
    sys.stderr.write("{}: {}\n".format(k,v))
sys.stderr.write("***************************************\n")

# MORE DIRS
## outputs
DIR_BASE       = abspath(config["basedir"])
DIR_RES        = join(DIR_BASE, "results")
DIR_LOGS       = join(DIR_BASE, "logs")
DIR_BENCHMARKS = join(DIR_BASE, "benchmarks")

# specific output dirs
DIR_MULTIQC    = join(DIR_RES, "00_multiqc")
DIR_TRIM       = join(DIR_RES, "01_trimmed")
DIR_COUNTS     = join(DIR_RES, "02_counts")

## define global Singularity image for reproducibility
## USE: "--use-singularity --use-conda" to run all jobs in container
singularity: config["singularity"]

## sample inputs
CFG_SAMPLESHEET  = abspath(config["samples"])

## =============================================================================
## TOOL PARAMETERS

# SALMON
CFG_SALMON_IDX   = os.path.abspath(config["salmon"]["idxdir"])
CFG_SALMON_EXTRA = config["salmon"]["extra"]

# GTF
CFG_GTF          = config["annotation"]["gtf"]
CFG_ELEMENT      = config["annotation"]["element"]
if config["annotation"]["geneversion"]:
    CFG_GENEVERSION = ""
else:
    CFG_GENEVERSION = "--noversion"

CFG_T2G = config["annotation"]["t2g"]
if not os.path.isfile(CFG_T2G):
    if not os.path.isfile(CFG_GTF):
        print("Either GTF or t2g file needed. EXIT.")
        sys.exit(1)
    else:
        # create from GTF
        CFG_T2G = join(DIR_COUNTS, "tx_gene_map.txt")
    
# multiqc
CFG_MULTIQC_COL  = config["multiqc"]["samplesheet_col_with_names"]

# DGEA contrasts
CFG_CONTRASTS    = config["diffexp"]["contrasts"]

# GSEA
CFG_ENRICH_GMT   = config["gsea"]["gmt"]
CFG_ENRICH_GCOL  = config["gsea"]["genecol"]
CFG_GSEA_NPERMS  = config["gsea"]["nperms"]

## Which analyses to perform?
CFG_TODO         = config["todo"]

## =============================================================================
## LOAD SAMPLES, checkif files exist

# validate samplesheet
samples = pd.read_csv(CFG_SAMPLESHEET, sep="\t").set_index("sample", drop=False)
validate(samples, schema=join(DIR_SCHEMAS, "samples.schema.yaml"))

## reading samplename from samplesheet
sys.stderr.write('Reading samples from samplesheet: "{}" ...\n'.format(CFG_SAMPLESHEET))

# test if sample in dir
for fname in samples["fq1"]:
    if not os.path.isfile(fname):
        sys.stderr.write("File '{}' from samplesheet fq1 can not be found. Make sure the file exists. Exit\n".format(fname))
        sys.exit(1)

for fname in samples["fq2"]:
    if pd.isnull(fname):
        continue
    if not os.path.isfile(fname):
        sys.stderr.write("File '{}' from samplesheet fq2 can not be found. Make sure the file exists. Exit\n".format(fname))
        sys.exit(1)

NUM_SAMPLES = len(samples["sample"])
sys.stderr.write('{} samples to process\n'.format(NUM_SAMPLES))

## =============================================================================
## FUNCTIONS
## =============================================================================
def get_fastq(wildcards):
    # does not return fq2 if it is not present
    return samples.loc[(wildcards.sample), ["fq1", "fq2"]].dropna()


def get_contrast(wildcards):
    return config["diffexp"]["contrasts"][wildcards.contrast]


def is_single_end(sample):
    return pd.isnull(samples.loc[(sample), "fq2"])


def get_trimmed(wildcards):
    if not is_single_end(**wildcards):
        # paired-end sample
        return expand(join(DIR_TRIM, "{sample}.{group}.fastq.gz"),
                      group=[1, 2], **wildcards)
    # single end sample, returns a list with one element
    return [join(DIR_TRIM, "{sample}.fastq.gz".format(**wildcards))]

    
## =============================================================================
## SETUP TARGETS
## =============================================================================
TARGETS = [join(DIR_COUNTS, "002_expr/tx.tpm.stats.txt.gz"),
           join(DIR_COUNTS, "002_expr/tx.tpm.gct.gz"),
           join(DIR_COUNTS, "002_expr/genes.tpm.stats.txt.gz"),
           join(DIR_COUNTS, "002_expr/genes.tpm.gct.gz")]

if "multiqc" in CFG_TODO:
    TARGETS += [join(DIR_MULTIQC, "multiqc.done")]

if "diffexpr" in CFG_TODO:
    TARGETS += expand([join(DIR_COUNTS, "004_diffexp/{contrast}.genes.diffexp.stats.tsv.gz"),
                       join(DIR_COUNTS, "004_diffexp/{contrast}.tx.diffexp.stats.tsv.gz"),
                       join(DIR_COUNTS, "004_diffexp/{contrast}.genes.ma-plot.pdf")],
                      contrast=CFG_CONTRASTS)
if "pca" in CFG_TODO:
    TARGETS += [join(DIR_COUNTS, "pca.genes.pdf")]

if "gsea" in CFG_TODO:
    if "diffexpr" in CFG_TODO:
        TARGETS += expand([join(DIR_COUNTS, "004_diffexp/{contrast}.genes.diffexp.stats.tsv.gz"),
                          join(DIR_COUNTS, "004_diffexp/{contrast}.tx.diffexp.stats.tsv.gz")],
                          contrast=CFG_CONTRASTS)
    else:
        TARGETS += expand([join(DIR_COUNTS, "004_diffexp/{contrast}.genes.diffexp.stats.tsv.gz"),
                           join(DIR_COUNTS, "004_diffexp/{contrast}.tx.diffexp.stats.tsv.gz"),
                           join(DIR_COUNTS, "004_diffexp/{contrast}.genes.ma-plot.pdf"),
                           join(DIR_COUNTS, "005_gsea/{contrast}.genes.diffexp.gsea-up.all.tsv.gz")],
                          contrast=CFG_CONTRASTS)

if config["entrez"]["file"]:
    if not os.path.isfile(config["entrez"]["file"]):
         sys.stderr.write("File '{}' can not be found. Make sure the file exists. Exit\n".format(config["entrez"]["file"]))
         sys.exit()
    else:
        TARGETS.append(join(DIR_COUNTS, "002_expr/entrez/genes.tpm.txt.gz"))
        
MQC_DIRS = [DIR_COUNTS]
if "fastp" in CFG_TODO or "bbduk" in CFG_TODO:
    func_salmon_get_data = get_trimmed
    MQC_DIRS.append(DIR_TRIM)
    # chosse the trimmer rules
    if "fastp" in CFG_TODO:
        include: join(DIR_RULES, "fastp.smk")
    else:
        include: join(DIR_RULES, "bbduk.smk")
else:
    func_salmon_get_data = get_fastq

## =============================================================================
## RULES
## =============================================================================
rule all:
    input:
        TARGETS

## ===================================================================
## QUANTIFY TX & GENES
        
rule salmon:
    input:
        # here I read the fq1 (and fq2) from samplesheet. wrapper takes care
        # to decide if pe or se run should be done or if files need to be decompressed
        sample=func_salmon_get_data,
        index=CFG_SALMON_IDX
    output:
        quant=join(DIR_COUNTS,'001_salmon/{sample}/quant.sf'),
        lib=join(DIR_COUNTS,'001_salmon/{sample}/lib_format_counts.json')
    log:
        join(DIR_LOGS, "salmon/{sample}.log")
    benchmark:
        join(DIR_BENCHMARKS, "salmon/{sample}.txt")
    params:
        libtype=config["salmon"]["libtype"],
        extra=CFG_SALMON_EXTRA
    threads: 16
    conda:
        join(DIR_ENVS, "salmon.yaml")
    wrapper:
        "file://wrapper/salmon_quant"


## ===================================================================
## EXPRESSION

# rule to create samplesheet with quant.sf path for tximport 
rule get_sample_quant_path:
    # we attach the path to the quant.sf file to the sample
    input:
        sheet=CFG_SAMPLESHEET,
        samples=expand(join(DIR_COUNTS,'001_salmon/{sample}/quant.sf'), sample=samples["sample"])  # to have it run after salmon has run
    output:
        join(DIR_COUNTS, "sample_list.txt")
    log:
        join(DIR_LOGS, "get_sample_quant_path.stderr")
    params:
        salmondir = join(DIR_COUNTS, "001_salmon"),
        script = join(DIR_SCRIPTS, "sample_list.py")
    shell:
        "python {params.script} {input.sheet} {params.salmondir} > {output} 2> {log}"

        
# we create tx_gene_map from gtf annotation for tximport
# only if not exists
rule get_tx_gene_map:
    input:
        CFG_GTF
    output:
        CFG_T2G
    log:
        join(DIR_LOGS, "get_tx_gene_map.stderr")
    benchmark:
        join(DIR_BENCHMARKS,"get_tx_gene_map.txt")
    params:
        script = join(DIR_SCRIPTS, "extract_txmap_from_gtf.py"),
        version=CFG_GENEVERSION,
        elem=CFG_ELEMENT
    shell:
        "python {params.script} {params.version} --element {params.elem} {input} > {output} 2> {log}"


## rule to get the Deseq2 objects and raw and normalised counts
rule deseq2_init:
    input:
        samplelist = join(DIR_COUNTS, "sample_list.txt"),
        t2g = CFG_T2G
    output:
        join(DIR_COUNTS, "003_deseq2/dds.tx.rds"),
        join(DIR_COUNTS, "003_deseq2/tx.counts.normalized.txt.gz"),
        join(DIR_COUNTS, "003_deseq2/tx.counts.raw.txt.gz"),
        join(DIR_COUNTS, "003_deseq2/dds.rds"),
        join(DIR_COUNTS, "003_deseq2/genes.counts.normalized.txt.gz"),
        join(DIR_COUNTS, "003_deseq2/genes.counts.raw.txt.gz")
    params:
        samples=config["samples"],
        paired=config["diffexp"]["paired"]
    conda:
        join(DIR_ENVS, "dexpr.yaml")
    log:
        join(DIR_LOGS, "deseq2/init.log")
    threads: 32
    script:
        join(DIR_SCRIPTS, "deseq2-init.R")


## rule to get TPM expr
rule get_expr_tpm:
    input:
        samplelist = join(DIR_COUNTS, "sample_list.txt"),
        t2g = CFG_T2G
    output:
        join(DIR_COUNTS, "002_expr/tx.tpm.txt.gz"),
        join(DIR_COUNTS, "002_expr/genes.tpm.txt.gz"),
        join(DIR_COUNTS, "002_expr/tx.counts.txt.gz"),
        join(DIR_COUNTS, "002_expr/genes.counts.txt.gz")
    log:
        join(DIR_LOGS, "get_expr_tpm.log")
    conda:
        join(DIR_ENVS, "dexpr.yaml")
    threads: 16
    params:
        scaling="lengthScaledTPM"  # for counts scaling, not TPM
    script:
        join(DIR_SCRIPTS, "get_expr_tpm.R")
        
        
## rule to make some stats for all tx
rule get_tx_stats:
    input:
        tpm=join(DIR_COUNTS, "002_expr/tx.tpm.txt.gz"),
        samples=CFG_SAMPLESHEET
    output:
        join(DIR_COUNTS, "002_expr/tx.tpm.stats.txt.gz")
    log:
        join(DIR_LOGS, "get_tx_stats.stderr")
    benchmark:
        join(DIR_BENCHMARKS, "get_tx_stats.txt")
    conda:
        join(DIR_ENVS, "pandas.yaml")
    params:
        script = join(DIR_SCRIPTS, "tx_stats.py")
    shell:
        "python {params.script} {input.tpm} {input.samples} 2> {log} | gzip > {output}"

        
#rule to make some stats for all genes
rule get_gene_stats:
    input:
        tpm=join(DIR_COUNTS, "002_expr/genes.tpm.txt.gz"),
        samples=CFG_SAMPLESHEET
    output:
        join(DIR_COUNTS, "002_expr/genes.tpm.stats.txt.gz")
    log:
        join(DIR_LOGS, "get_gene_stats.stderr")
    benchmark:
        join(DIR_BENCHMARKS, "get_gene_stats.txt")
    conda:
        join(DIR_ENVS, "pandas.yaml")
    params:
        script = join(DIR_SCRIPTS, "tx_stats.py")
    shell:
        "python {params.script} {input.tpm} {input.samples} 2> {log} | gzip > {output}"

        
#rule to make gct files for igv visualisation
rule get_gct_tx:
    input:
        join(DIR_COUNTS, "002_expr/tx.tpm.txt.gz"),
        CFG_GTF
    output:
        join(DIR_COUNTS, "002_expr/tx.tpm.gct.gz")
    log:
        join(DIR_LOGS, "get_gct_tx.stderr")
    benchmark:
        join(DIR_BENCHMARKS, "get_gct_tx.txt")
    params:
        script = join(DIR_SCRIPTS, "build_gct.py"),
        extra = r''
    shell:
        "python {params.script} {params.extra} {input[0]} {input[1]} | gzip > {output} 2> {log}"

        
#rule to make gct files for igv visualisation
rule get_gct_gene:
    input:
        join(DIR_COUNTS, "002_expr/genes.tpm.txt.gz"),
        CFG_GTF
    output:
        join(DIR_COUNTS, "002_expr/genes.tpm.gct.gz")
    log:
        join(DIR_LOGS, "get_gct_gene.stderr")
    benchmark:
        join(DIR_BENCHMARKS, "get_gct_gene.txt")
    params:
        script = join(DIR_SCRIPTS, "build_gct.py"),
        extra = r'--gene {}'.format(CFG_GENEVERSION)
    shell:
        "python {params.script} {params.extra} {input[0]} {input[1]} | gzip > {output} 2> {log}"


rule attach_entrezgene:
    input:
        join(DIR_COUNTS, "tx_gene_map.txt"),
        config["entrez"]["file"]
    output:
        join(DIR_COUNTS, "tx_gene_map_entrez.txt")
    benchmark:
        join(DIR_BENCHMARKS, "attach_entrezgene.txt")
    log:
        join(DIR_LOGS, "attach_entrezgene.log")
    params:
        script=join(DIR_SCRIPTS, "attach_entrez_tx_map.py"),
        extra="--type {}".format(config["entrez"]["type"])
    shell:
       "python {params.script} {params.extra} {input[0]} {input[1]} > {output} 2> {log}"

       
rule get_entrezgene:
    input:
        join(DIR_COUNTS, "002_expr/genes.tpm.txt.gz"),
        join(DIR_COUNTS, "tx_gene_map_entrez.txt")
    output:
        join(DIR_COUNTS, "002_expr/entrez/genes.tpm.txt.gz")
    benchmark:
        join(DIR_BENCHMARKS, "get_entrezgene.txt")
    log:
        join(DIR_LOGS, "get_entrezgene.log")
    params:
        script=join(DIR_SCRIPTS, "subselect_entrezgene.py"),
        extra=""
    shell:
        "python {params.script} {params.extra} {input[0]} {input[1]} -o {output} > {log} 2>&1"

## ===================================================================
## pca
rule pca:
    input:
        join(DIR_COUNTS, "003_deseq2/dds.tx.rds"),
        join(DIR_COUNTS, "003_deseq2/dds.rds")
    output:
        report(join(DIR_COUNTS, "pca.tx.pdf"), join(DIR_REPORT, "pca.tx.rst")),
        report(join(DIR_COUNTS, "pca.genes.pdf"), join(DIR_REPORT, "pca.genes.rst"))
        #join(DIR_COUNTS, "counts-normalized.txt.gz")
    params:
        pca_labels=config["pca"]["labels"],
        function=config["pca"]["function"]
    conda:
        join(DIR_ENVS, "dexpr.yaml")
    log:
        join(DIR_LOGS, "pca.log")
    script:
        join(DIR_SCRIPTS, "plot-pca.R")

        
## ===================================================================
## diffexp  
rule deseq2:
    input:
        join(DIR_COUNTS, "003_deseq2/dds.rds")
    output:
        table=join(DIR_COUNTS, "004_diffexp/{contrast}.genes.diffexp.tsv.gz"),
        ma_plot=report(join(DIR_COUNTS, "004_diffexp/{contrast}.genes.ma-plot.pdf"),
                       join(DIR_REPORT, "ma.genes.rst"))
    params:
        contrast=get_contrast,
        filterFun=config["diffexp"]["filterFun"]
    conda:
       join(DIR_ENVS, "dexpr.yaml")
    log:
        join(DIR_LOGS, "deseq2/{contrast}.diffexp.log")
    benchmark:
        join(DIR_BENCHMARKS, "deseq2.{contrast}.txt")
    threads: 16
    script:
        join(DIR_SCRIPTS, "deseq2.R")


rule deseq2_tx:
    input:
        join(DIR_COUNTS, "003_deseq2/dds.tx.rds")
    output:
        table=join(DIR_COUNTS, "004_diffexp/{contrast}.tx.diffexp.tsv.gz"),
        ma_plot=report(join(DIR_COUNTS, "004_diffexp/{contrast}.tx.ma-plot.pdf"),
                          join(DIR_REPORT, "ma.tx.rst")),
    params:
        contrast=get_contrast,
        filterFun=config["diffexp"]["filterFun"]
    conda:
       join(DIR_ENVS, "dexpr.yaml")
    log:
        join(DIR_LOGS, "deseq2_tx/{contrast}.diffexp.log")
    benchmark:
        join(DIR_BENCHMARKS, "deseq2_tx.{contrast}.txt")
    threads: 16
    script:
        join(DIR_SCRIPTS, "deseq2.R")
        

## attach gene stats
rule attach_genes_stats:
    input:
        de=join(DIR_COUNTS, "004_diffexp/{contrast}.genes.diffexp.tsv.gz"),
        map=CFG_T2G,
        stats=join(DIR_COUNTS, "002_expr/genes.tpm.stats.txt.gz")
    output:
        report(join(DIR_COUNTS, "004_diffexp/{contrast}.genes.diffexp.stats.tsv.gz"),
               join(DIR_REPORT, "diffexp.tx.rst"))
    log:
        join(DIR_LOGS, "attach_gene_stats_{contrast}.log")
    params:
        script=join(DIR_SCRIPTS, "join.py")
    shell:
        "python {params.script} "
        "{input.de} {input.map} {input.stats} "
        "2> {log} | gzip > {output}"

        
## attach tx stats
rule attach_tx_stats:
    input:
        de=join(DIR_COUNTS, "004_diffexp/{contrast}.tx.diffexp.tsv.gz"),
        map=CFG_T2G,
        stats=join(DIR_COUNTS, "002_expr/tx.tpm.stats.txt.gz")
    output:
        report(join(DIR_COUNTS, "004_diffexp/{contrast}.tx.diffexp.stats.tsv.gz"),
               join(DIR_REPORT, "diffexp.genes.rst"))
    log:
        join(DIR_LOGS, "attach_tx_stats_{contrast}.log")
    params:
        script=join(DIR_SCRIPTS, "join.py")
    shell:
        "python {params.script} --tx "
        "{input.de} {input.map} {input.stats} "
        "2> {log} | gzip > {output}"
        

        
## ===================================================================
## gsea
rule gsea_msigdb:
    input:
        join(DIR_COUNTS, "004_diffexp/{contrast}.genes.diffexp.stats.tsv.gz")
    output:
        report(join(DIR_COUNTS, "005_gsea/{contrast}.genes.diffexp.gsea-up.all.tsv.gz")),
        report(join(DIR_COUNTS, "005_gsea/{contrast}.genes.diffexp.gsea-dn.all.tsv.gz")),
        report(join(DIR_COUNTS, "005_gsea/{contrast}.genes.diffexp.gsea-up.selected.tsv.gz")),
        report(join(DIR_COUNTS, "005_gsea/{contrast}.genes.diffexp.gsea-dn.selected.tsv.gz"))
    log:
        join(DIR_LOGS, "gsea_msigdb/{contrast}.log")
    conda:
        join(DIR_ENVS, "clusterprofiler.yaml")
    params:
        script=join(DIR_SCRIPTS, "gsea_msigdb_symbols.R")
    shell:
        "zcat {input} | cut -f 1-9 | Rscript --vanilla --slave {params.script} "
        "{CFG_ENRICH_GCOL} {CFG_ENRICH_GMT} {CFG_GSEA_NPERMS} "
        "{output[0]} {output[1]} {output[2]} {output[3]} "
        "2> {log}"
        

## ===================================================================
## multiqc
rule mutiqc_samplenames:
    input:
        CFG_SAMPLESHEET
    output:
        temp(join(DIR_MULTIQC, "multiqc-samplenames.txt"))
    shell:
        """
        echo -e "sampleid\tsamplename" > {output};
        cat {input} | cut -f 1,{CFG_MULTIQC_COL} >> {output}
        """

        
rule multiqc:
    input:
        join(DIR_MULTIQC, "multiqc-samplenames.txt"),
        expand(join(DIR_COUNTS, '001_salmon/{sample}/quant.sf'), sample=samples["sample"])
    output:
        touch(join(DIR_MULTIQC, "multiqc.done"))
    log:
        join(DIR_LOGS, "multiqc.log"),
    conda:
        join(DIR_ENVS, "multiqc.yaml")
    params:
        dirs=" ".join(MQC_DIRS)
    shell:
        "multiqc -f --sample-names {input[0]} -o {DIR_MULTIQC} {params.dirs} "
        "> {log} 2>&1"
